/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2019/1/13 10:18:41                           */
/*==============================================================*/


drop table if exists sys_admin;

drop index idx_user_id on sys_admin_role;

drop index idx_role_id on sys_admin_role;

drop index id on sys_admin_role;


alter table sys_admin_role 
   drop foreign key FK_SYS_ADMI_REFERENCE_SYS_ADMI;

alter table sys_admin_role 
   drop foreign key FK_SYS_ADMI_REFERENCE_SYS_ROLE;

drop table if exists sys_admin_role;

drop index id on sys_privilege;

drop table if exists sys_privilege;

drop table if exists sys_role;

drop index idx_privilege_id on sys_role_permission;

drop index idx_role_id on sys_role_permission;

drop index id on sys_role_permission;


alter table sys_role_permission 
   drop foreign key FK_SYS_ROLE_REFERENCE_SYS_PRIV;

alter table sys_role_permission 
   drop foreign key FK_SYS_ROLE_REFERENCE_SYS_ROLE;

drop table if exists sys_role_permission;

drop index idx_login_id on vln_access_token;

drop index idx_token on vln_access_token;

drop table if exists vln_access_token;

drop table if exists vln_ads;

drop index idx_usr_id on vln_answer;

drop index idx_qsn_id on vln_answer;


alter table vln_answer 
   drop foreign key FK_VLN_ANSW_REFERENCE_VLN_QUES;

alter table vln_answer 
   drop foreign key FK_VLN_ANSW_REFERENCE_VLN_USER;

drop table if exists vln_answer;

drop index idx_usr_id on vln_app_message;


alter table vln_app_message 
   drop foreign key FK_VLN_APP__REFERENCE_VLN_USER;

drop table if exists vln_app_message;

drop table if exists vln_group;

drop table if exists vln_news_column;


alter table vln_news_flush 
   drop foreign key FK_VLN_NEWS_REFERENCE_VLN_NEWS;

drop table if exists vln_news_flush;

drop index idx_publish_date on vln_news_live;

drop index idx_created on vln_news_live;

drop index idx_status on vln_news_live;

drop index idx_clm_id on vln_news_live;


alter table vln_news_live 
   drop foreign key FK_VLN_NEWS_REFERENCE_VLN_NEWS;

drop table if exists vln_news_live;

drop index idx_stock_code on vln_opinion;

drop index idx_publish_date on vln_opinion;

drop index idx_usr_id on vln_opinion;

drop index idx_clm_id on vln_opinion;


alter table vln_opinion 
   drop foreign key FK_VLN_OPIN_REFERENCE_VLN_NEWS;

alter table vln_opinion 
   drop foreign key FK_VLN_OPIN_REFERENCE_VLN_USER;

drop table if exists vln_opinion;

drop table if exists vln_parameter;

drop index idx_usr_id on vln_question;

drop index idx_nws_id on vln_question;


alter table vln_question 
   drop foreign key FK_VLN_QUES_REFERENCE_VLN_NEWS;

alter table vln_question 
   drop foreign key FK_VLN_QUES_REFERENCE_VLN_USER;

drop table if exists vln_question;

drop index idx_ref_id on vln_raw_news;

drop table if exists vln_raw_news;


alter table vln_relative_stock 
   drop foreign key FK_VLN_RELA_REFERENCE_VLN_NEWS;

drop table if exists vln_relative_stock;

drop index idx_stock_name on vln_stock;

drop index idx_stock_code on vln_stock;

drop table if exists vln_stock;

drop index idx_stk_code on "vln_stock_financial data";

drop table if exists "vln_stock_financial data";

drop index idx_sfd_id on "vln_stock_financial data_detail";


alter table "vln_stock_financial data_detail" 
   drop foreign key FK_VLN_STOC_REFERENCE_VLN_STOC;

drop table if exists "vln_stock_financial data_detail";


alter table vln_stock_group 
   drop foreign key FK_VLN_STOC_REFERENCE_VLN_STOC;

alter table vln_stock_group 
   drop foreign key FK_VLN_STOC_REFERENCE_VLN_GROU;

drop table if exists vln_stock_group;

drop index idx_stk_code on vln_stock_manager;

drop table if exists vln_stock_manager;

drop index idx_stk_code on vln_stock_shareholding;

drop table if exists vln_stock_shareholding;


alter table vln_stock_watchlist 
   drop foreign key FK_VLN_STOC_REFERENCE_VLN_USER;

drop table if exists vln_stock_watchlist;


alter table vln_suggestion 
   drop foreign key FK_VLN_SUGG_REFERENCE_VLN_USER;

drop table if exists vln_suggestion;

drop index idx_nick_name on vln_user;

drop index idx_union_id on vln_user;

drop index idx_open_id on vln_user;

drop index idx_phone_no on vln_user;

drop index idx_user_name on vln_user;

drop table if exists vln_user;

drop table if exists vln_verify_code;

/*==============================================================*/
/* User: caimi                                                  */
/*==============================================================*/
create user caimi;

/*==============================================================*/
/* Table: sys_admin                                             */
/*==============================================================*/
create table sys_admin
(
   adm_id               bigint not null  comment '',
   adm_login_name       varchar(256) not null  comment '登录名',
   adm_display_name     varchar(64)  comment '显示名',
   adm_password         varchar(256) not null  comment '密码',
   adm_is_pwd_expired   int  comment '密码是否过期',
   adm_status           tinyint not null  comment '0：正常，1:暂停，2：删除',
   adm_remark           varchar(1024)  comment '备注',
   created_on           datetime not null  comment '创建时间',
   modified_on          datetime  comment '修改时间',
   primary key (adm_id)
);

alter table sys_admin comment '系统管理员';

/*==============================================================*/
/* Table: sys_admin_role                                        */
/*==============================================================*/
create table sys_admin_role
(
   id                   bigint not null  comment '',
   adm_id               bigint  comment '',
   rle_id               bigint  comment '',
   primary key (id)
);

/*==============================================================*/
/* Index: id                                                    */
/*==============================================================*/
create index id on sys_admin_role
(
   id
);

/*==============================================================*/
/* Index: idx_role_id                                           */
/*==============================================================*/
create index idx_role_id on sys_admin_role
(
   rle_id
);

/*==============================================================*/
/* Index: idx_user_id                                           */
/*==============================================================*/
create index idx_user_id on sys_admin_role
(
   adm_id
);

/*==============================================================*/
/* Table: sys_privilege                                         */
/*==============================================================*/
create table sys_privilege
(
   pvl_id               bigint not null  comment '',
   parent_id            bigint  comment '上级权限id',
   privilege_name       varchar(255)  comment '权限名称',
   privilege_code       varchar(64)  comment '权限代码',
   privilege_desc       varchar(255)  comment '权限描述说明',
   privilege_type       tinyint  comment '权限类型
             1：菜单，2：按钮，3：数据',
   pvl_status           tinyint default 0  comment '0正常 2:删除',
   created_by           varchar(255)  comment '',
   last_updated_by      varchar(255)  comment '',
   created_on           datetime not null  comment '创建时间',
   modified_on          datetime  comment '修改时间',
   primary key (pvl_id)
);

alter table sys_privilege comment '记录用户权限相关的后台菜单，按钮';

/*==============================================================*/
/* Index: id                                                    */
/*==============================================================*/
create index id on sys_privilege
(
   pvl_id
);

/*==============================================================*/
/* Table: sys_role                                              */
/*==============================================================*/
create table sys_role
(
   rle_id               bigint not null  comment '',
   rle_name             varchar(64) not null  comment '角色名',
   rle_status           varchar(16)  comment '角色状态',
   rle_remark           varchar(1024)  comment '备注',
   created_on           datetime not null  comment '创建时间',
   modified_on          datetime  comment '修改时间',
   primary key (rle_id)
);

alter table sys_role comment '管理员角色';

/*==============================================================*/
/* Table: sys_role_permission                                   */
/*==============================================================*/
create table sys_role_permission
(
   id                   bigint not null  comment '',
   pvl_id               bigint  comment '',
   rle_id               bigint  comment '',
   primary key (id)
);

/*==============================================================*/
/* Index: id                                                    */
/*==============================================================*/
create index id on sys_role_permission
(
   id
);

/*==============================================================*/
/* Index: idx_role_id                                           */
/*==============================================================*/
create index idx_role_id on sys_role_permission
(
   rle_id
);

/*==============================================================*/
/* Index: idx_privilege_id                                      */
/*==============================================================*/
create index idx_privilege_id on sys_role_permission
(
   pvl_id
);

/*==============================================================*/
/* Table: vln_access_token                                      */
/*==============================================================*/
create table vln_access_token
(
   tkn_id               bigint not null  comment '',
   tkn_login_id         varchar(64)  comment '登录id',
   tkn_token            varchar(256)  comment 'token',
   tkn_create_time      datetime  comment '创建时间',
   tkn_expired_time     datetime  comment '过期时间',
   tkn_source_id        varchar(128)  comment '登录客户端IP',
   tkn_terminal_id      varchar(128)  comment '客户端唯一号',
   tkn_refresh_token    varchar(256)  comment '过期刷新token',
   primary key (tkn_id)
)
engine = InnoDB;

alter table vln_access_token comment '用户登录token';

/*==============================================================*/
/* Index: idx_token                                             */
/*==============================================================*/
create unique index idx_token on vln_access_token
(
   tkn_token
);

/*==============================================================*/
/* Index: idx_login_id                                          */
/*==============================================================*/
create unique index idx_login_id on vln_access_token
(
   tkn_login_id
);

/*==============================================================*/
/* Table: vln_ads                                               */
/*==============================================================*/
create table vln_ads
(
   ads_id               bigint not null  comment '',
   ads_location         varchar(16)  comment 'STK_PICKER		：金股页
             PORTFOLIO		：组合页
             INVESTMENT	：理财页',
   ads_terminal_type    varchar(16) not null default 'ANDROID'  comment '显示终端',
   ads_title            varchar(128)  comment '标题',
   ads_img_url          varchar(128)  comment '显示图片',
   ads_action_type      varchar(16)  comment '跳转类型
             href：链接地址
             mui：手机端UI',
   ads_action_link      varchar(1024)  comment '跳转地址',
   ads_sort_index       tinyint  comment '排序
             值越大越靠前',
   ads_remark           varchar(1024)  comment '备注',
   created_on           datetime not null  comment '创建时间',
   modified_on          datetime  comment '修改时间',
   primary key (ads_id)
)
engine = InnoDB;

alter table vln_ads comment '广告页';

/*==============================================================*/
/* Table: vln_answer                                            */
/*==============================================================*/
create table vln_answer
(
   ans_id               bigint not null  comment '',
   qsn_id               bigint  comment '',
   usr_id               bigint  comment '用户id',
   ans_content          varchar(1024) not null  comment '回答内容',
   ans_status           tinyint default 0  comment '回答状态',
   ans_replier          bigint  comment '',
   primary key (ans_id)
);

alter table vln_answer comment '回答表';

/*==============================================================*/
/* Index: idx_qsn_id                                            */
/*==============================================================*/
create index idx_qsn_id on vln_answer
(
   qsn_id
);

/*==============================================================*/
/* Index: idx_usr_id                                            */
/*==============================================================*/
create index idx_usr_id on vln_answer
(
   usr_id
);

/*==============================================================*/
/* Table: vln_app_message                                       */
/*==============================================================*/
create table vln_app_message
(
   msg_id               bigint not null  comment '',
   usr_id               bigint  comment '用户id',
   msg_type             varchar(16) not null  comment '消息类型',
   msg_title            varchar(512)  comment '标题',
   msg_content          longtext  comment '内容',
   msg_is_read          smallint default 0  comment '0：未读
             1：已读',
   msg_action_link      varchar(1024)  comment '跳转地址',
   msg_action_type      varchar(16) not null default 'NONE'  comment 'NONE,HREF,MUI',
   primary key (msg_id)
)
engine = InnoDB;

/*==============================================================*/
/* Index: idx_usr_id                                            */
/*==============================================================*/
create index idx_usr_id on vln_app_message
(
   usr_id
);

/*==============================================================*/
/* Table: vln_group                                             */
/*==============================================================*/
create table vln_group
(
   grp_id               bigint not null  comment '',
   grp_name             varchar(256)  comment '分组名称',
   grp_desc             varchar(512)  comment '分组说明',
   grp_status           tinyint  comment '状态',
   grp_sort_index       int  comment '排序索引',
   created_on           datetime  comment '创建时间',
   modified_on          datetime  comment '修改时间',
   primary key (grp_id)
);

/*==============================================================*/
/* Table: vln_news_column                                       */
/*==============================================================*/
create table vln_news_column
(
   clm_id               bigint not null  comment '',
   parent_id            bigint  comment '父栏目',
   clm_code             varchar(32)  comment '代码',
   clm_display_name     varchar(32)  comment '显示名',
   clm_group            varchar(32)  comment '栏目组',
   clm_status           tinyint  comment '状态',
   clm_remark           varchar(1024)  comment '备注',
   created_on           datetime not null  comment '创建时间',
   modified_on          datetime  comment '修改时间',
   primary key (clm_id)
);

alter table vln_news_column comment '资讯栏目';

/*==============================================================*/
/* Table: vln_news_flush                                        */
/*==============================================================*/
create table vln_news_flush
(
   nws_id               bigint not null  comment '',
   clm_id               bigint  comment '栏目id',
   nws_title            varchar(512)  comment '标题',
   nws_keywords         varchar(128)  comment '关键字',
   nws_brief            varchar(1024)  comment '摘要',
   nws_content          longtext  comment '内容',
   nws_source           varchar(64)  comment '来源',
   nws_publish_dte      datetime  comment '发布日期',
   nws_status           tinyint default 0  comment '0：待处理，1：已处理',
   nws_view_count       varchar(32)  comment '阅读数量',
   nws_stick_top        tinyint  comment '是否置顶',
   nws_color            varchar(16)  comment '标题显示颜色',
   created_on           datetime not null  comment '创建时间',
   modified_on          datetime  comment '修改时间',
   primary key (nws_id)
);

alter table vln_news_flush comment '快讯';

/*==============================================================*/
/* Table: vln_news_live                                         */
/*==============================================================*/
create table vln_news_live
(
   nws_id               bigint not null  comment '',
   clm_id               bigint  comment '',
   nws_title            varchar(512)  comment '标题',
   nws_keywords         varchar(512)  comment '关键字',
   nws_brief            varchar(512)  comment '摘要',
   nws_content          varchar(512)  comment '内容',
   nws_thumbnail_url    varchar(512)  comment '缩略图',
   nws_video_url        varchar(512)  comment '视频地址',
   nws_publish_dte      datetime  comment '发布日期',
   nws_status           tinyint default 0  comment '0：预告，1：直播，2：完成，3：历史',
   nws_view_count       varchar(32)  comment '阅读数量',
   nws_stick_top        tinyint  comment '是否置顶',
   nws_color            varchar(16)  comment '标题显示颜色',
   nws_stock_code       varchar(16)  comment '相关股票',
   nws_relate_news      varchar(1024)  comment '相关资讯',
   created_on           datetime not null  comment '创建时间',
   modified_on          datetime  comment '修改时间',
   primary key (nws_id)
);

alter table vln_news_live comment '直播新闻表';

/*==============================================================*/
/* Index: idx_clm_id                                            */
/*==============================================================*/
create index idx_clm_id on vln_news_live
(
   clm_id
);

/*==============================================================*/
/* Index: idx_status                                            */
/*==============================================================*/
create index idx_status on vln_news_live
(
   nws_status
);

/*==============================================================*/
/* Index: idx_created                                           */
/*==============================================================*/
create index idx_created on vln_news_live
(
   created_on
);

/*==============================================================*/
/* Index: idx_publish_date                                      */
/*==============================================================*/
create index idx_publish_date on vln_news_live
(
   nws_publish_dte
);

/*==============================================================*/
/* Table: vln_opinion                                           */
/*==============================================================*/
create table vln_opinion
(
   opn_id               bigint not null  comment '',
   clm_id               bigint  comment '',
   usr_id               bigint  comment '用户id',
   opn_title            varchar(512)  comment '标题',
   opn_brief            varchar(1024)  comment '摘要',
   opn_content          longtext  comment '内容',
   opn_publish_dte      datetime  comment '发布日期',
   opn_status           tinyint default 0  comment '0：待处理，1：已处理',
   opn_stock_code       varchar(16)  comment '',
   opn_view_count       varchar(32)  comment '阅读数量',
   opn_stick_top        tinyint  comment '是否置顶',
   opn_color            varchar(16)  comment '标题显示颜色',
   opn_thumbnail_url    varchar(512)  comment '',
   opn_video_url        varchar(512)  comment '',
   created_on           datetime not null  comment '创建时间',
   modified_on          datetime  comment '修改时间',
   primary key (opn_id)
);

/*==============================================================*/
/* Index: idx_clm_id                                            */
/*==============================================================*/
create index idx_clm_id on vln_opinion
(
   clm_id
);

/*==============================================================*/
/* Index: idx_usr_id                                            */
/*==============================================================*/
create index idx_usr_id on vln_opinion
(
   usr_id
);

/*==============================================================*/
/* Index: idx_publish_date                                      */
/*==============================================================*/
create index idx_publish_date on vln_opinion
(
   opn_publish_dte
);

/*==============================================================*/
/* Index: idx_stock_code                                        */
/*==============================================================*/
create index idx_stock_code on vln_opinion
(
   opn_stock_code
);

/*==============================================================*/
/* Table: vln_parameter                                         */
/*==============================================================*/
create table vln_parameter
(
   prm_id               bigint not null  comment '',
   prm_name             varchar(32) not null  comment '参数显示名',
   prm_key              varchar(32) not null  comment '参数key',
   prm_value            varchar(1024) not null  comment '参数值',
   prm_group            varchar(32)  comment '参数分组',
   created_on           datetime not null  comment '创建时间',
   modified_on          datetime  comment '修改时间',
   primary key (prm_id)
);

alter table vln_parameter comment '参数配置表';

/*==============================================================*/
/* Table: vln_question                                          */
/*==============================================================*/
create table vln_question
(
   qsn_id               bigint not null  comment '',
   nws_id               bigint  comment '',
   usr_id               bigint not null  comment '用户id',
   qsn_content          varchar(1024) not null  comment '问题内容',
   qsn_status           tinyint default 0  comment '状态：0：待回答，1：已回答',
   primary key (qsn_id)
);

/*==============================================================*/
/* Index: idx_nws_id                                            */
/*==============================================================*/
create index idx_nws_id on vln_question
(
   nws_id
);

/*==============================================================*/
/* Index: idx_usr_id                                            */
/*==============================================================*/
create index idx_usr_id on vln_question
(
   usr_id
);

/*==============================================================*/
/* Table: vln_raw_news                                          */
/*==============================================================*/
create table vln_raw_news
(
   nws_id               bigint not null  comment '',
   ref_id               varchar(32)  comment '',
   nws_title            varchar(512)  comment '标题',
   nws_brief            varchar(1024)  comment '摘要',
   nws_content          longtext  comment '内容',
   nws_source           varchar(64)  comment '来源',
   nws_publish_dte      datetime  comment '发布日期',
   nws_create_date      datetime  comment '创建日期',
   nws_url              varchar(128)  comment '',
   nws_status           tinyint default 0  comment '0：待处理，1：已处理',
   nws_type             varchar(32)  comment '类型',
   primary key (nws_id)
);

/*==============================================================*/
/* Index: idx_ref_id                                            */
/*==============================================================*/
create index idx_ref_id on vln_raw_news
(
   ref_id
);

/*==============================================================*/
/* Table: vln_relative_stock                                    */
/*==============================================================*/
create table vln_relative_stock
(
   lnk_id               bigint not null  comment '',
   relate_id            bigint  comment '',
   stk_code             varchar(16)  comment '',
   primary key (lnk_id)
);

/*==============================================================*/
/* Table: vln_stock                                             */
/*==============================================================*/
create table vln_stock
(
   stk_id               bigint not null  comment '',
   stk_code             varchar(16)  comment '股票代码',
   stk_name             varchar(16)  comment '股票名称',
   stk_logo             varchar(512)  comment '',
   stk_keys             varchar(32)  comment '拼音缩写',
   stk_shareholders     int  comment '股东人数',
   stk_circulating_shares_rate decimal(5,1)  comment '流通股占比',
   created_on           datetime not null  comment '创建时间',
   modified_on          datetime  comment '修改时间',
   primary key (stk_id)
);

/*==============================================================*/
/* Index: idx_stock_code                                        */
/*==============================================================*/
create unique index idx_stock_code on vln_stock
(
   stk_code
);

/*==============================================================*/
/* Index: idx_stock_name                                        */
/*==============================================================*/
create unique index idx_stock_name on vln_stock
(
   stk_name
);

/*==============================================================*/
/* Table: "vln_stock_financial data"                            */
/*==============================================================*/
create table "vln_stock_financial data"
(
   sfd_id               bigint not null  comment '',
   stk_code             varchar(16)  comment '股票代码',
   sdf_data_title       varchar(128)  comment '财务数据标题',
   created_on           datetime  comment '创建时间',
   modified_on          datetime not null  comment '修改时间',
   primary key (sfd_id)
);

/*==============================================================*/
/* Index: idx_stk_code                                          */
/*==============================================================*/
create index idx_stk_code on "vln_stock_financial data"
(
   stk_code
);

/*==============================================================*/
/* Table: "vln_stock_financial data_detail"                     */
/*==============================================================*/
create table "vln_stock_financial data_detail"
(
   dtl_id               bigint not null  comment '',
   sfd_id               bigint  comment '',
   dtl_title            varchar(64)  comment '指标名称',
   dtl_data_value       varchar(16)  comment '指标数值',
   dtl_data_rate        varchar(16)  comment '指标增长率',
   created_on           datetime  comment '创建时间',
   modified_on          datetime  comment '修改时间',
   primary key (dtl_id)
);

/*==============================================================*/
/* Index: idx_sfd_id                                            */
/*==============================================================*/
create index idx_sfd_id on "vln_stock_financial data_detail"
(
   sfd_id
);

/*==============================================================*/
/* Table: vln_stock_group                                       */
/*==============================================================*/
create table vln_stock_group
(
   id                   bigint not null  comment '',
   stk_id               bigint  comment '',
   grp_id               bigint  comment '',
   sort_index           int  comment '0',
   primary key (id)
);

alter table vln_stock_group comment '股票分组关系';

/*==============================================================*/
/* Table: vln_stock_manager                                     */
/*==============================================================*/
create table vln_stock_manager
(
   id                   bigint not null  comment '',
   stk_code             varchar(16)  comment '股票代码',
   mgr_real_name        varchar(256)  comment '姓名',
   mgr_position         varchar(256)  comment '职务',
   mgr_head_pic         varchar(512)  comment '头像',
   mgr_resume           longtext  comment '个人简介',
   created_on           datetime not null  comment '创建时间',
   modified_on          datetime  comment '修改时间',
   primary key (id)
);

/*==============================================================*/
/* Index: idx_stk_code                                          */
/*==============================================================*/
create index idx_stk_code on vln_stock_manager
(
   stk_code
);

/*==============================================================*/
/* Table: vln_stock_shareholding                                */
/*==============================================================*/
create table vln_stock_shareholding
(
   id                   bigint not null  comment '',
   stk_code             varchar(16)  comment '股票代码',
   ssh_real_name        varchar(256)  comment '持股人姓名',
   ssh_weight           decimal(5,3)  comment '持股比例',
   created_on           datetime not null  comment '创建时间',
   modified_on          datetime  comment '修改时间',
   primary key (id)
);

/*==============================================================*/
/* Index: idx_stk_code                                          */
/*==============================================================*/
create index idx_stk_code on vln_stock_shareholding
(
   stk_code
);

/*==============================================================*/
/* Table: vln_stock_watchlist                                   */
/*==============================================================*/
create table vln_stock_watchlist
(
   lst                  bigint not null  comment '',
   usr_id               bigint  comment '用户id',
   stk_code             varchar(16)  comment '',
   sort_index           int  comment '排序索引，数值越大越靠前',
   created_on           datetime not null  comment '创建时间',
   modified_on          datetime  comment '修改时间',
   primary key (lst)
);

/*==============================================================*/
/* Table: vln_suggestion                                        */
/*==============================================================*/
create table vln_suggestion
(
   sgs_id               bigint not null  comment '',
   usr_id               bigint  comment '用户id',
   sgs_content          varchar(1024) not null  comment '反馈内容',
   sgs_status           varchar(16) not null  comment '状态',
   sgs_reply            varchar(1024)  comment '回复内容',
   sgs_reply_on         datetime  comment '回复时间',
   sgs_reply_by         varchar(64)  comment '回复人',
   created_on           datetime not null  comment '创建时间',
   modified_on          datetime  comment '修改时间',
   primary key (sgs_id)
)
engine = InnoDB;

/*==============================================================*/
/* Table: vln_user                                              */
/*==============================================================*/
create table vln_user
(
   usr_id               bigint not null  comment '用户id',
   usr_name             varchar(64)  comment '用户名',
   usr_password         varchar(256)  comment '密码',
   usr_nick_name        varchar(128)  comment '',
   usr_phone            varchar(32)  comment '手机号',
   usr_open_id          varchar(128)  comment '第三方登录id',
   usr_union_id         varchar(128)  comment '第三方登录unionId',
   usr_head_img         varchar(256)  comment '',
   usr_role             varchar(128)  comment 'JC：韭菜，TG:投顾，DK：大咖，FXS：分析师，SSGS_：上市公司。',
   usr_status           tinyint  comment '',
   created_on           datetime  comment '创建时间',
   modified_on          datetime  comment '修改时间',
   create_by            varchar(32)  comment '',
   primary key (usr_id)
);

alter table vln_user comment '用户表';

/*==============================================================*/
/* Index: idx_user_name                                         */
/*==============================================================*/
create unique index idx_user_name on vln_user
(
   usr_name
);

/*==============================================================*/
/* Index: idx_phone_no                                          */
/*==============================================================*/
create unique index idx_phone_no on vln_user
(
   usr_phone
);

/*==============================================================*/
/* Index: idx_open_id                                           */
/*==============================================================*/
create unique index idx_open_id on vln_user
(
   usr_open_id
);

/*==============================================================*/
/* Index: idx_union_id                                          */
/*==============================================================*/
create index idx_union_id on vln_user
(
   usr_union_id
);

/*==============================================================*/
/* Index: idx_nick_name                                         */
/*==============================================================*/
create index idx_nick_name on vln_user
(
   usr_nick_name
);

/*==============================================================*/
/* Table: vln_verify_code                                       */
/*==============================================================*/
create table vln_verify_code
(
   id                   bigint not null  comment '',
   phone_no             national varchar(16) not null  comment '手机号',
   vrf_code             national varchar(16) not null  comment '验证码',
   vrf_expire_time      datetime not null  comment '过期时间',
   vrf_send_count       tinyint default 0  comment '当天发送次数',
   created_on           datetime not null  comment '创建时间',
   modified_on          datetime  comment '修改时间',
   primary key (id)
);

